#include "Auxiliary.h"

//developed by Joao Maia and Jose Oliveira, T6G03

/******************************************************************
*  Main function for validation of user input                     *
*                                                                 *
*  @Param: - type can be 's' for string, 'd' for decimal or 'c'   *
*          for a single char                                      *
*          - optinal except = true (will throw exception if       *
*          ctrl+z is pressed                                      *
*          - the id of the menu to return if ctrl+z is pressed    *
*  @Return: user input or "^Z" if ctrl+z is pressed and except =  *
*          false                                                  *
******************************************************************/
string input(char type, bool except, int menu)
{
	stringstream ss;
	string str, final_input;
	int size = 0;

	while(true) // While enter is not pressed and the input is not valid
	{
		// Waiting for key press
		int key = _getch();

		// If the key pressed corresponds to a letter or space
		if(isValidKey(key, type)) 
		{
			ss << (char) key;			// Adding letter to the buffer
			size++;						// Incrementing input size
			if(type == 'p')				// If password 
				cout << '*';
			else
				cout << (char) key;		// Printing on screen
		}

		// If the key is backspace
		else if(key == '\b' && size != 0)
		{
			str = ss.str();
			ss = stringstream();				// stringstream reset
			ss << str.substr(0, str.size()-1);	// Adding input string without the last char
			size--;								// Decrementing input size
			cout<<"\b \b";						// Erase char on the screen
		}

		// If the key is enter
		else if(key == 0x0D)
		{
			while(ss >> ws >> str >> ws)		// Extracting each word, without spaces
				final_input += str + ' ';		// Adding to final string + space
			if(final_input.size() != 0)			// If the final string is not empty, then the user input is complete
				break;
			else
			{
				final_input = "";				// Resetting final string
				ss = stringstream();			// Resetting buffer
				size = 0;
			}
		}
		// Ctrl + Z
		else if(key == 0x1A)
			return quit(except, menu, type);
	}

	cout << endl; // '\n' which corresponds to the last enter

	// Returning final string without the final space
	return final_input.substr(0, final_input.size()-1);
}

/******************************************************************
*  Used for input and validation of the gender                    *
*                                                                 *
*  @Param: - optional parameter except = true, throws exception   *
*          if ctrl+z is pressed. Else returns a '^Z' char         *
*          - menu to return of ctrl+z is pressed                  *
*  @Return: 'M', 'F' or '^Z'                                      *
******************************************************************/
char genderInput(bool except, int menu)
{
	// Default
	char gender = 'N';

	while(true)
	{
		// Waiting for key press
		int key = _getch();

		if(key == 0x6D || key == 0x66) // Converting 'm' or 'f' to upper case
			key -= 0x20;

		if((key == 0x4D || key == 0x46) && gender == 'N') // M or F
		{
			gender = key;		// If the gender is valid
			cout << gender;		// Printing to the screen
		}

		// Backspace
		else if(key == '\b' && gender != 'N')
		{
			gender = 'N';		// Resetting gender
			cout << "\b \b";	// Erasing char from the screen
		}

		// Enter
		else if(key == 0xD && gender != 'N')
			break;

		// Ctrl + Z
		else if(key == 0x1A)
			return quit(except, menu)[0];
	}

	// '\n' corresponding to the last enter
	cout << endl;

	return gender;
}

/******************************************************************
*  Check if the key is valid                                      *
*                                                                 *
*  @Param: - key pressed by the user                              *
*          - type of input ('s' for string, 'd' for decimal, 'c'  *
*          for a single char                                      *
*  @Return: true if the key is valid                              *
******************************************************************/
bool isValidKey(int key, char type)
{
	// Strings
	if(type == 's')
		return ((key >= 'A' && key <= 'Z')		// A-Z
		|| (key >= 'a' && key <= 'z')			// a-z
		|| key == 0x20);						// space
	// Numbers
	else if(type == 'd')
		return ((key >= '0' && key <= '9'));	// 0-9  
	// Both s and d
	else
		return ((key >= '0' && key <= '9')		// 0-9
		|| (key >= 'A' && key <= 'Z')			// A-Z
		|| (key >= 'a' && key <= 'z')			// a-z
		|| key == 0x20);						// space
}

/******************************************************************
*  Used to quit from the user input                               *
*                                                                 *
*  @Param: - if exception == true, throws and exception           *
*  @Exception: RadioException("Quit", -1, true)                   *
*  @Return: string "^Z" if exception == false                     *
******************************************************************/
string quit(bool exception, int menu, char type)
{
	if(exception)
		throw RadioException("Quit", menu, true);

	cout << "^Z" << endl;

	if(type != 'd')
	{
		return "^Z";
	}
	else
		return "-1";
}

/******************************************************************
*  Split the string by comma (',') and returns a vector of        *
* strings                                                         *
*                                                                 *
*  @Param: - line: string to split                                *
*  @Return: vector of splitted strings                            *
******************************************************************/
vector<string> sepComma(string line)
{
	vector<string> final_str;
	string param;
	istringstream ss(line);

	// Separating string by comma and adding each word to a vector
	while(getline(ss, param, ','))
		final_str.push_back(param);	

	return final_str;
}

/******************************************************************
*  Convert an int to a bool                                       *
*                                                                 *
*  @Param: - integer (0 or 1)                                     *
*  @Return: true if num = 1                                       *
******************************************************************/
bool intToBool(int num)
{
	if(num == 0)
		return false;
	else 
		return true;
}

/******************************************************************
*  Get the current cursor position                                *
*                                                                 *
*  @Return: COORD object                                          *
******************************************************************/
COORD getCursorPosition()
{
	// Getting handle to the standard console
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	// Getting Screen Buffer Info
	CONSOLE_SCREEN_BUFFER_INFO info;
	GetConsoleScreenBufferInfo(hConsole, &info);

	// Selecting position to return
	return COORD(info.dwCursorPosition);
}

/******************************************************************
*  Get the current cursor position and clear the screen line      *
*                                                                 *
*  @Param: - position: cursor position destination                *
*          - size: number of characters to erase                  *
******************************************************************/
void goToCoord(COORD position, int size)
{
	// Gettin handle to the standard console
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	// Moving cursor to the position
	SetConsoleCursorPosition(hConsole, position);

	if(size != -1)
	{
		// Clearing characters
		cout << setfill(' ') << setw(size) << " ";

		// Moving cursor to the initial position
		SetConsoleCursorPosition(hConsole, position);
	}
}

/******************************************************************
*  Clear the console screen                                       *
*  By: JAS - Abr/2011                                             *
******************************************************************/
void clrscr(void)
{
	COORD upperLeftCorner = {0,0};
	DWORD charsWritten;
	DWORD conSize;
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO  csbi;

	GetConsoleScreenBufferInfo(hCon, &csbi);
	conSize = csbi.dwSize.X * csbi.dwSize.Y;

	// fill with spaces
	FillConsoleOutputCharacter(hCon, TEXT(' '), conSize, upperLeftCorner, &charsWritten);
	GetConsoleScreenBufferInfo(hCon, &csbi);
	FillConsoleOutputAttribute(hCon, csbi.wAttributes, conSize, upperLeftCorner, &charsWritten);

	// cursor to upper left corner
	SetConsoleCursorPosition(hCon, upperLeftCorner);
}
#include "RadioStation.h"


//developed by Joao Maia and Jose Oliveira, T6G03

/*========================= LIFECYCLE ===========================*/

/******************************************************************
*  Default constructor                                            *
******************************************************************/
RadioStation::RadioStation()
{
	// Setting default name
	mName = "Unknown";

	// Empty vectors
	mMusics = vector< Music* >(0);
	mTopTen = vector< Music* >(0);
	mUsers = vector< User* >(0);

	// Creating default user
	mpCurrentUser = new User("admin", 0, 'N', "admin");
	mpCurrentUser->SetAdmin(true);
	mUsers.push_back(mpCurrentUser);

	// Load previous data
	ReadUsersFile();
	ReadMusicsFile();
	ReadTopTenFile();
	ReadLikesFile();
	ReadDislikesFile();

	// Loading each User playlist
	for(unsigned i = 0; i < mUsers.size(); i++)
		ReadPlaylistFile(*mUsers[i]);
}

/******************************************************************
*  Simple constructor                                             *
******************************************************************/
RadioStation::RadioStation(string name)
{
	// Setting name
	mName = name;

	// Empty vectors
	mMusics = vector< Music* >(0);
	mTopTen = vector< Music* >(0);
	mUsers = vector< User* >(0);

	// Creating default user
	mpCurrentUser = new User("admin", 0, 'N', "admin");
	mpCurrentUser->SetAdmin(true);
	mUsers.push_back(mpCurrentUser);

	// Load previous data
	ReadUsersFile();
	ReadMusicsFile();
	ReadTopTenFile();
	ReadLikesFile();
	ReadDislikesFile();

	// Loading each User playlist
	for(unsigned i = 0; i < mUsers.size(); i++)
		ReadPlaylistFile(*mUsers[i]);
}

/******************************************************************
*  Destructor                                                     *
******************************************************************/
RadioStation::~RadioStation()
{
	// Resetting data on file
	ofstream out_file("radioStationMusics.csv", ios::out);
	out_file << "";
	out_file.close();

	UpdateUsersFile();
	UpdateTopTenFile();
	UpdateLikesFile();
	UpdateDislikesFile();

	// Deleting all users (freeing memory and saving data to files)
	for(unsigned i = 0; i < mUsers.size(); i++)
		delete mUsers[i];

	// Deleting all musics (freeing memory and saving data to files)
	for(unsigned i = 0; i < mMusics.size(); i++)
		delete mMusics[i];
}

/*========================= OPERATIONS ==========================*/

/******************************************************************
*  Main                                                           *
******************************************************************/
void RadioStation::Run()
{
	// Preview
	mCurrentMenu = 0;

	while(true)
	{
		try
		{
			switch(mCurrentMenu)
			{
			case 0x00: Preview(); break;									// Preview

			case 0x01: LoginMenu(); break;									// Login Menu
			case 0x11: LoginUser(); break;									// Login User
			case 0x12: RegisterUser(); break;								// Register User
			case 0x13: return;												// Quit

			case 0x02: MainMenu(); break;									// Main Menu
			case 0x21: ShowTopTen(); _getch(); break;						// User Menu
			case 0x22: ShowUserPlaylist(*mpCurrentUser); _getch(); break;	// Radio Menu
			case 0x23: SearchMenu(); break;									// Search
			case 0x24: mCurrentMenu=0x01; LoginMenu(); break;				// Back to Login Menu
			case 0x29: AdminMenu(); break;									// Admin Menu
			}
		}
		catch(const RadioException& e)
		{
			if(!e.Quit())
			{
				// Display error message
				cout << e.GetMsg() << endl;
				_getch();
			}
			// Change menu
			if(e.Menu() != -1)
				mCurrentMenu = e.Menu();
		}
		// Clearing screen
		clrscr();
	}
}

/******************************************************************
*  Print to the console a welcoming screen                        *
*  ID: 0x00                                                       *
******************************************************************/
void RadioStation::Preview()
{
	cout << endl<< endl<< endl<< endl<< endl<< endl<< endl<< endl<< endl<< setw(25) << " " << "Projeto de Programacao" << endl << endl << setw(25)<< " "<< "Radio Station" << endl;

	cout << endl << endl<< endl<< endl<< endl<< endl<< endl<< endl<< endl<< endl << setw(63) << " " << "Programmed by:" << endl << endl <<setw(65) << " " << "Joao Maia" << endl << setw(65) << " " << "Jose Oliveira";

	// Waiting for key press
	int key = _getch();

	// Login Menu
	mCurrentMenu = 1;
}

/******************************************************************
*  Login menu                                                     *
*  In this menu, the user can decide whether to login, register   *
* or quit                                                         *
*  ID: 0x01                                                       *
******************************************************************/
void RadioStation::LoginMenu()
{
	// Screen
	cout << "Welcome to PombosPorAcaso Radiostation!" << endl << "Are you a registered user?" << endl << endl << "1. Login" << endl << "2. Register" << endl << "3. Quit" << endl;

	while(true)
	{
		// Waiting for key press
		int option = _getch() - 0x30;

		// Selecting options
		if(option >= 1 && option <= 3)
		{
			mCurrentMenu = option + 0x10;
			return;
		}
	}
}

/******************************************************************
*  Main menu                                                      *
*  This menu gives access to the User Menu, Radio Menu and Admin  *
* menu (if the user is admin)                                     *
*  ID: 0x02                                                       *
******************************************************************/
void RadioStation::MainMenu()
{
	cout << "1. Top Ten List" << endl << "2. Show Playlist" << endl << "3. Search for a music"  << endl << "4. Quit" << endl;

	if(mpCurrentUser->IsAdmin())
		cout << endl << "9. Admin Menu" << endl;

	while(true)
	{
		// Waiting for key press
		int option = _getch() - 0x30;

		// Selecting menu to show
		if((option >= 1 && option <= 4) || (option == 9 && mpCurrentUser->IsAdmin()))
		{
			mCurrentMenu = option + 0x20;
			return;
		}
	}
}

/******************************************************************
*  Admin menu                                                     *
*  In this menu, the admin can manage the list of music tracks of *
* the radio station                                               *
*  ID: 0x23                                                       *
******************************************************************/
void RadioStation::AdminMenu()
{
	cout << "1. Add Music" << endl << "2. Edit Music" << endl << "3. Change Availability" << endl << "4. Give Prize" << endl << "5. Quit" << endl;

	while(true)
	{
		// Waiting for key press
		int option = _getch() - 0x30;

		switch(option)
		{
		case 1: clrscr(); AddMusic(); return;				// Add Track
		case 2: clrscr(); EditMusic(); return;				// Display Playlist
		case 3: clrscr(); ChangeStatus(); return;			// Display Playlist
		case 4: clrscr(); GivePrize(); return;				// Give Prize
		case 5: clrscr(); mCurrentMenu = 0x02; return;		// Quit (Main Menu)
		}
	}
}

/******************************************************************
*  Login                                                          *
*  Ask for the account name (and password?)                       *
*  Check if user already exists                                   *
*  Set the current user                                           *
*  ID: 0x11                                                       *
******************************************************************/
void RadioStation::LoginUser()
{
	// User name input
	cout << "User: ";
	string user = input('s', true, 0x01);

	// Password input
	cout << "Password: ";
	string password = input('p', true, 0x01);

	// Searching for the user
	vector< User* >::iterator iter;

	for (iter = mUsers.begin(); iter != mUsers.end(); iter++)
		if ((*iter)->GetName() == user)				// If the user is found
			if((*iter)->GetPassword() == password)	// If the both password match
			{
				SetCurrentUser(**iter);
				throw RadioException("Successful Login!", 0x02);	// Successful Login (go to Main Menu)
			}
			else
				throw RadioException("Wrong Password!", 0x01);		// Wrong Password (go to Login Menu)

	throw RadioException("User doesn't exist.", 0x01);				// User Doens't exists (go to Login Menu)
}

/******************************************************************
*  Register user                                                  *
*  To have access to the radio station facilities a user must     *
* register himself at the application                             *
*  ID: 0x12                                                       *
******************************************************************/
void RadioStation::RegisterUser()
{
	// User name input
	cout << "Enter a new username: ";
	string user = input('s', true, 0x01);

	// Checking if the user already exists
	if(IsUser(user))
		throw RadioException("That username already exists.", 0x01);	// User already exists (go to Login Menu)

	else
	{
		// Password Input
		cout << endl << "Set a password: ";
		string password = input('p', true, 0x01);

		// Age input
		cout << endl << "Enter your age: ";
		int age = stoi(input('d', true, 0x01));

		// Gender Input
		cout << endl << "Enter your gender (M/F): ";
		char gender = genderInput(true, 0x01);

		mUsers.push_back(new User(user, age, gender, password));

		mpCurrentUser = mUsers.back();

		throw RadioException("Registration completed!", 0x01);		// Registration completed (go to Login Menu)
	}
}

/******************************************************************
*  Ask for the parameter to use in the search                     *
******************************************************************/
void RadioStation::SearchMenu()
{
	cout << "Which parameter do you want to use?" << endl << "1. Artist" << endl << "2. Author" << endl << "3. Year" << endl << "4. Show all" << endl << "5. Quit" << endl;

	while(true)
	{
		// Waiting for key press
		int option = _getch();

		if(option >= '1' && option <= '5')
		{
			clrscr();

			int counter = 0;

			switch(option)
			{
			case '1': cout << "Artist: "; break;
			case '2': cout << "Author: "; break;
			case '3': cout << "Year: "; break;
			case '4':
				PrintHeader();
				for(unsigned i = 0; i < mMusics.size(); i++)
				{
					if(mMusics[i]->IsAvailable())
					{
						mMusics[i]->ShowMusic();
						counter += 1;				// Incrementing counter for the number of music tracks available

						if(mMusics[i]->IsAvailable() && mpCurrentUser->IsAdmin())
							cout << setw(10) << "Yes";

						else if(mpCurrentUser->IsAdmin())
							cout << setw(10) << "No";

						cout << endl;
					}
				}
				// If there isn't any Music track available
				if(counter == 0)
				{
					clrscr();
					throw RadioException("There isn't any Music track available in the Radio Station");
				}

				break;
			case '5': mCurrentMenu = 0x02; return;
			}

			if(option != '4')
				Search(option - 0x30);

			Music &track = SelectMusic();

			while(true)
				SelectMusicMenu(track);

			return;
		}
	}
}

/******************************************************************
*  Show the Music tracks of a given Artist / Author / Year        *
*                                                                 *
*  @Param: - param: 1 to select by artist                         *
*                   2 to select by author                         *
*                   3 to select by year                           *
******************************************************************/
void RadioStation::Search(int param) 
{
	string parameter;

	// Deciding which method of input to use
	if(param == 1 || param == 2)
	{
		// User input
		parameter = input('s', true, 0x02);

		// Converting to upper case (all the music tracks string members start with upper case
		parameter[0] = toupper(parameter[0]);
	}
	else
		parameter = input('d', true, 0x02);

	vector<vector<Music*>::const_iterator> tracks;

	// Search by the selected parameter
	switch(param)
	{
	case 1:
		for(vector<Music*>::const_iterator location = mMusics.begin(); location != mMusics.end(); location++)
			if((*location)->GetArtist() == parameter)
				tracks.push_back(location);
		break;
	case 2:
		for(vector<Music*>::const_iterator location = mMusics.begin(); location != mMusics.end(); location++)
			if((*location)->GetAuthor() == parameter)
				tracks.push_back(location);
		break;
	case 3:
		for(vector<Music*>::const_iterator location = mMusics.begin(); location != mMusics.end(); location++)
			if((*location)->GetYear() == stoi(parameter))
				tracks.push_back(location);
		break;
	}

	// If no music tracks were found
	if(tracks.empty())
		throw RadioException("No Music tracks were found!", 0x23); 

	// Clrearing screen
	clrscr();

	PrintHeader();
	int counter = 0;

	for(unsigned i = 0; i < tracks.size(); i++)
	{
		// If the user is Admin
		if(mpCurrentUser->IsAdmin())
		{
			// Show music
			(*tracks[i])->ShowMusic();

			if((*tracks[i])->IsAvailable())
			{ 
				cout << setw(10) << left << "Yes" << endl;
				counter += 1;
			}
			else
				cout << setw(10) << left << "No" << endl;
		}
		// Else if the music is available
		else if((*tracks[i])->IsAvailable())
		{
			(*tracks[i])->ShowMusic();
			cout << endl;
			counter += 1;
		}
	}

	if(counter == 0)
	{
		clrscr();
		throw RadioException("Music track wasn't found!");
	}
}

/******************************************************************
*  Orders musics by any key: title, year, artist or genre         *
******************************************************************/
void RadioStation::OrderMusics() 
{
	clrscr();

	// Checking if the radio station has Music tracks
	if(mMusics.empty())
		throw RadioException("The Radio Station hasn't got any music tracks yet!", 0x02);	// No Music tracks were found (go to Main menu)

	// Adding available Music tracks to a vector
	vector< Music* > tracks;

	if(mCurrentMenu != 0x29)	// Admin Menu
	{
		for(unsigned i = 0; i < mMusics.size(); i++)
			if(mMusics[i]->IsAvailable())
				tracks.push_back(mMusics[i]);
	}
	else
		tracks = mMusics;		// If the user is admin and is on the Admin Menu

	cout << "Which parameter do you want to search for?" << endl << "1. Title" << endl << "2. Artist" << endl << "3. Year" << endl << "4. Music Genre" << endl << "5. Quit" << endl;

	while(true)
	{
		// Waiting for key press
		int key = _getch() - 0x30;

		// Deciding which sort method to use
		switch(key)
		{
		case 1: sort(tracks.begin(), tracks.end(), CompareTitle()); break;		// Title
		case 2: sort(tracks.begin(), tracks.end(), CompareArtist()); break;		// Artist
		case 3: sort(tracks.begin(), tracks.end(), CompareYear()); break;		// Year
		case 4: sort(tracks.begin(), tracks.end(), CompareMusicGenre()); break;	// Music Genre
		case 5: mCurrentMenu = 0x02;	 return;									// Quit (go to User or Radio menu)
		}

		// Displaying ordered Music track list
		if(key >= 1 && key <= 4)
		{
			clrscr();

			// Printing header
			PrintHeader();

			// Showing each Music track info
			for(unsigned i = 0; i < tracks.size(); i++)
			{

				if(mpCurrentUser->IsAdmin())
				{
					tracks[i]->ShowMusic();

					if(tracks[i]->IsAvailable()) 
						cout << setw(10) << left << "Yes" << endl;

					else
						cout << setw(4) << left << "No" << endl;
				}
				else if(tracks[i]->IsAvailable())
				{
					tracks[i]->ShowMusic();
					cout << endl;
				}
			}
			return;
		}
	}

}

/******************************************************************
*  Search in the radio station's playlist the music with the      *
* given ID                                                        *
*                                                                 *
*  @Return: a reference to the radio station's music with the     *
*          given ID                                               *
******************************************************************/
Music& RadioStation::SelectMusic() 
{
	cout << "Music Track ID: ";

	// Get screen cursor position
	COORD position = getCursorPosition();

	while(true)
	{
		// Waiting for ID input
		int id = stoi(input('d', true, mCurrentMenu));

		// Searching for the Music track
		for(unsigned i = 0; i < mMusics.size(); i++)
		{
			if(mMusics[i]->GetId() == id && (mMusics[i]->IsAvailable() || mpCurrentUser->IsAdmin()))
			{
				// Cleaning "No Music track was found." message
				cout << "\n                         ";
				goToCoord(position); cout << endl;
				return *mMusics[i];
			}
			// If the Music track is found and is not available 
			else if(mMusics[i]->GetId() == id && !(mMusics[i]->IsAvailable()))      
				break;
		}

		// If no Music track is found
		cout << "\nNo Music track was found.\n";

		// Setting screen cursor position to its initial position
		goToCoord(position, id/10 + 1);
	}
}

/******************************************************************
*  Display all the music track information                        *
*                                                                 *
*  @Param: - the track which was selected                         *
******************************************************************/
void RadioStation::SelectMusicMenu(Music &track)
{
	// Get music track ID
	int track_id = track.GetId();
	bool like, dislike = false;

	// Checking if the user likes the music track
	vector<int> likes = mpCurrentUser->GetLikes();
	vector<int>::iterator match = find(likes.begin(), likes.end(), track_id);

	like = match != likes.end() ? true : false;

	// Checking if the user dislikes the music track
	if(!like)
	{
		vector<int> dislikes = mpCurrentUser->GetDislikes();
		match = find(dislikes.begin(), dislikes.end(), track_id);

		dislike = match != dislikes.end() ? true : false;
	}
	// Clearing screen
	clrscr();

	// Printing music track information
	cout << "Title: " << track.GetTitle() << endl;
	cout << "Artist: " << track.GetArtist() << endl;
	cout << "Author: " << track.GetAuthor() << endl;
	cout << "Album: " << track.GetAlbum() << endl;
	cout << "Music Genre: " << track.GetMusicGenre() << endl;
	cout << "Year: " << track.GetYear() << endl << endl;

	// Informing if the user likes or dislikes the music track
	if(like) cout << "You like this music track" << endl << endl;
	else if(dislike) cout << "You dislike this music track" << endl << endl;

	// Get user playlist
	vector< const Music* > playlist = mpCurrentUser->GetPlaylist();

	// Searching for the pointer to the Music track
	vector< const Music* >::iterator location = find(playlist.begin(), playlist.end(), &track); 

	// If the Music track isn't in the playlist, then giving the option to add it
	if(location == playlist.end())
		cout << "1. Add Track" << endl;
	else	// Else to remove it
		cout << "1. Remove Track" << endl;

	if(dislike) cout << "2. Like" << endl;
	else if(like) cout << "2. Dislike" << endl;
	else cout << "2. Like" << endl << "3. Dislike" << endl;

	cout  << "Q. Quit" << endl;

	while(true)
	{
		// Waiting for key press
		int option = _getch();

		switch(option)
		{
		case '1': if(location == playlist.end())
				  {
					  mpCurrentUser->AddMusic(track); return;                 // Add Track
				  }
				  else if(playlist.empty())
					  throw RadioException("Your Playlist is empty!", 0x02); // Empty playlist (go to Main Menu)

				  else
				  {
					  mpCurrentUser->RemoveMusic(track); return;		// Remove Track	
				  }
		case '2': if(!(like || dislike)) //if the user never liked or disliked this music track
				  {
					  Like(track); _getch(); return;	// Display Playlist
				  }
				  else if(dislike)
				  {
					  Like(track); _getch(); return;	// Display Playlist
				  }
				  else
				  {
					  Dislike(track); _getch(); return;
				  }
		case '3': if(!(like || dislike))
				  {
					  Dislike(track); _getch(); return;
				  }
				  break;
		case 'Q': throw RadioException("Quit", 0x02, true);						// Quit (Main Menu)
		case 'q': throw RadioException("Quit", 0x02, true);
		}
	}

}

/******************************************************************
*  Generate, randomly, the set of musics played in a given period *
*  This set may be reinitialized by the administrator             *
******************************************************************/
void RadioStation::GenerateMusics()
{
	if(mMusics.size() < 10)
		throw RadioException("The Radio Station needs to have at least 10 Music tracks");

	int random_id;

	// Initializing seed
	srand((unsigned) time(0));


	for(int i = 0; i < 480; i++)  // a day has 24h, if we consider the average length of a track to be 3 min, in 24h we can reproduce 480 tracks
	{
		random_id = rand() % mMusics.size();

		// Searching
		vector< Music* >::iterator location = find_if(mMusics.begin(), mMusics.end(), FindId(mMusics[random_id]->GetId()));

		//Incrementing times playing
		IncTimesPlayed(**location); 
	}
}

/******************************************************************
*  Each time that a new set of musics is generated, we have to    *
* increment the times each music iss played                       *
*                                                                 *
*  @Param: - rTrack is a reference to a music from the radio      *
*          station's playlist                                     *
******************************************************************/
void RadioStation::IncTimesPlayed(Music &rTrack)
{
	// Getting Times Played
	int times_played = rTrack.GetTimesPlayed();

	// Incrementing Times Played
	times_played++;

	// Setting Times Played
	rTrack.SetTimesPlayed(times_played);
}

/******************************************************************
*  At the end of the day we need to see which user has more music *
* tracks with more hits, in order to give a prize                 *
******************************************************************/
void RadioStation::GivePrize()
{
	// Calculating number of times each music track played during a period of 24 hours
	GenerateMusics();

	// Cloning vector of User pointers
	vector< User* > users(mUsers);

	// Sorting vector according to the score
	sort(users.begin(), users.end(), CompareUserScore());

	// Showing the winner
	cout << "The winner is: " << users[0]->GetName() << endl;

	_getch();
}

/******************************************************************
*  Each time a "like"/"dislike" is changed the top ten list must  *
* be checked/updated                                              *
******************************************************************/
void RadioStation::UpdateTopTen()
{
	// Adding available Music tracks to a vector
	vector< Music* > tracks;

	for(unsigned i = 0; i < mMusics.size(); i++)
		if(mMusics[i]->IsAvailable())
			tracks.push_back(mMusics[i]);

	// Sorting tracks by score (difference between likes and dislikes)
	sort(tracks.begin(), tracks.end(), CompareScore());

	// Resetting mTopTen
	mTopTen = vector<Music*>();

	// Adding tracks to mTopTen
	for(unsigned i = 0, size = tracks.size(); i < size && i < 10; i++)
		mTopTen.push_back(tracks[i]);
}

/******************************************************************
*  Print to the console the Top Ten list                          *
******************************************************************/
void RadioStation::ShowTopTen() 
{
	UpdateTopTen();

	// Checking if the radio station has Music tracks
	if(mTopTen.empty())
		throw RadioException("The Radio Station hasn't got any music tracks yet!", 0x02);	// No Music tracks were found(go to Main Menu)

	else
	{
		// Printing header
		PrintHeader();

		// Printing each Music track info
		for(unsigned i = 0; i < mTopTen.size(); i++)
		{
			if(mpCurrentUser->IsAdmin())
			{
				mTopTen[i]->ShowMusic();

				if(mTopTen[i]->IsAvailable())
					cout << setw(10) << left << "Yes" << endl;
				else
					cout << setw(10) << left << "No" << endl;
			}
			else
			{
				mTopTen[i]->ShowMusic();
				cout << endl;
			}
		}

		mCurrentMenu=0x02;
	}
}

/******************************************************************
*  ADMIN ONLY                                                     *
*  Add a music track to the RadioStation's playlist               *
*  Ask admin for music name, title, artist, author, album, genre  *
* and year                                                        *                                                      *
******************************************************************/
void RadioStation::AddMusic()
{
	// User input and validation. Converting first char to upper case
	cout << "Title: ";			string title = input('s', true, 0x29);			title[0] = toupper(title[0]);
	cout << "Artist: ";			string artist = input('s', true, 0x29);			artist[0] = toupper(artist[0]);
	cout << "Author: ";			string author = input('s', true, 0x29);			author[0] = toupper(author[0]);
	cout << "Album: ";			string album = input('s', true, 0x29);			album[0] = toupper(album[0]);
	cout << "Music Genre: ";	string music_genre = input('s', true, 0x29);	music_genre[0] = toupper(music_genre[0]);
	cout << "Year: ";			int year = stoi(input('d', true, 0x29));

	// Checking if the music track already exists
	vector< Music* >::iterator match = find_if(mMusics.begin(), mMusics.end(), FindMusicTrack(title, artist, album, year));

	if(match != mMusics.end())
		throw RadioException("Music track already exists!", 0x29);

	// Adding to the radio playlist if it doesn't exist
	mMusics.push_back(new Music(title, artist, author, album, music_genre, year,0,0,true));
}

/******************************************************************
*  ADMIN ONLY                                                     *
*  To edit a radio station's track                                *
******************************************************************/
void RadioStation::EditMusic()
{
	// Showing a ordered list of Music tracks
	OrderMusics();

	// Selecting Music track
	Music& track = SelectMusic();

	cout << endl << "Press ctrl+Z to keep the value" << endl;

	// False -> Will not throw an exception case Ctrl+Z is pressed
	cout << "Title: ";			string title = input('s', false);			title[0] = toupper(title[0]);	
	cout << "Artist: ";			string artist = input('s', false);			artist[0] = toupper(artist[0]);
	cout << "Author: ";			string author = input('s', false);			author[0] = toupper(author[0]);
	cout << "Album: ";			string album = input('s', false);			album[0] = toupper(album[0]);
	cout << "Music Genre: ";	string music_genre = input('s', false);		music_genre[0] = toupper(music_genre[0]);
	cout << "Year: ";			int year = stoi(input('d', false));		

	vector< Music* >::iterator match = find_if(mMusics.begin(), mMusics.end(), FindMusicTrack(title, artist, album, year));

	if(match == mMusics.end())
	{
		track.SetTitle(title);
		track.SetArtist(artist);
		track.SetAuthor(author);
		track.SetAlbum(album);
		track.SetMusicGenre(music_genre);
		track.SetYear(year);

		cout << "Music track successfuly edited!" << endl;
	}
	else
		cout << "Music track already exists!" << endl;

	_getch();
}

/******************************************************************
*  ADMIN ONLY                                                     *
*  To signal as available/unvailable                              *
******************************************************************/
void RadioStation::ChangeStatus()
{
	// Showing a ordered list of Music tracks
	OrderMusics();

	// Selecting Music track
	Music& track = SelectMusic();

	// Determining if the music track is available or not
	bool is_available = track.IsAvailable();

	// If it is available
	if(is_available)
	{
		// Changing to not available
		track.SetAvailable(false);

		// Get track ID
		int track_id = track.GetId();

		// Removing the track from the users' playlist
		for(unsigned i = 0; i < mUsers.size(); i++)
		{
			// Get user playlist
			vector< const Music* > playlist = mUsers[i]->GetPlaylist();

			// Search for the music track
			vector< const Music* >::iterator location = find_if(playlist.begin(), playlist.end(), FindId(track_id));

			// If the music track is found, erasing it from the user's playlist
			if(location != playlist.end())
			{
				playlist.erase(location);
				mUsers[i]->SetPlaylist(playlist);
			}
		}
	}

	// If it is not, changing to available
	else
		track.SetAvailable(true);

	UpdateTopTen();
}

/******************************************************************
*  Show all the user's music tracks                               *
*                                                                 *
*  @Param: - rUser is a reference to a radio station's user       *
******************************************************************/
void RadioStation::ShowUserPlaylist(const User &rUser) 
{
	// Clearing the screen
	clrscr();

	// Checking if the playlist is empty
	if(rUser.GetPlaylist().empty())
		throw RadioException("Your playlist is empty!", 0x02);	// Empty playlist (go to Main Menu)

	// Printing header
	PrintHeader();

	// Showing playlist
	rUser.ShowPlaylist();

	mCurrentMenu=0x02;
}

/******************************************************************
*  Increment Like counter                                         *
*                                                                 *
*  @Param: - rTrack is a reference to a music from the radio      *
*          station's playlist                                     * 
******************************************************************/
void RadioStation::Like(Music &rTrack)
{
	// Get track ID and user dislikes
	int track_id = rTrack.GetId();
	vector<int> dislikes = mpCurrentUser->GetDislikes();

	// Searching for the track ID in the dislikes vector
	vector<int>::iterator location = find(dislikes.begin(), dislikes.end(), track_id);

	// If the track ID is found, then the users dislikes the music track
	if(location != dislikes.end())
	{
		dislikes.erase(location);				// Removing track ID from the dislikes vector
		mpCurrentUser->SetDislikes(dislikes);	// Setting dislikes
	}

	// Adding like to user list
	vector<int> likes = mpCurrentUser->GetLikes();
	likes.push_back(track_id);
	mpCurrentUser->SetLikes(likes);

	// Get, increment and set likes
	rTrack.SetLikes(rTrack.GetLikes() + 1);

	// Update Top Ten
	UpdateTopTen();

	cout << endl << "You like this music" << endl << endl;
}

/******************************************************************
*  Increment Dislike counter                                      *
*                                                                 *
*  @Param: - rTrack is a reference to a music from the radio      *
*          station's playlist                                     *
******************************************************************/
void RadioStation::Dislike(Music &rTrack)
{
	// Get track ID and user likes
	int track_id = rTrack.GetId();
	vector<int> likes = mpCurrentUser->GetLikes();

	// Searching for the track ID in the likes vector
	vector<int>::iterator location = find(likes.begin(), likes.end(), track_id);

	// If the track ID is found, then the users likes the music track
	if(location != likes.end())
	{
		likes.erase(location);			// Removing track ID from the likes vector
		mpCurrentUser->SetLikes(likes);	// Setting likes
	}

	// Adding dislike to user list
	vector<int> dislikes = mpCurrentUser->GetDislikes();
	dislikes.push_back(track_id);
	mpCurrentUser->SetDislikes(dislikes);

	// Get, increment and set likes
	rTrack.SetDislikes(rTrack.GetDislikes() + 1);

	// Update Top Ten
	UpdateTopTen();

	cout << endl << "You do not like this music" << endl << endl;
}

/******************************************************************
*  Create/update "likes.csv" file                                 *
******************************************************************/
void RadioStation::UpdateLikesFile()
{
	// Opening "likes.csv" to write
	ofstream out_file("likes.csv", ios::out);

	for(unsigned i = 0; i < mUsers.size(); i++)
	{
		vector<int> likes = mUsers[i]->GetLikes();

		// Writing user ID
		out_file << mUsers[i]->GetId() << " ";

		// Writing music tracks ID
		for(unsigned j = 0; j < likes.size(); j++)
			out_file << likes[j] << ',';

		out_file << endl;
	}

	// Closing "likes.csv"
	out_file.close();
}

/******************************************************************
*  Read "likes.csv" file                                          *
******************************************************************/
void RadioStation::ReadLikesFile()
{
	string line;

	// Opening "likes.csv" to read
	ifstream in_file("likes.csv", ios::in);

	// Reading file
	while(getline(in_file, line))
	{
		int user_id;
		stringstream stream(line);

		// Extracting user ID
		stream >> user_id;

		// Searching for the user in the radio station's users vector
		vector< User* >::iterator user = find_if(mUsers.begin(), mUsers.end(), FindUserById(user_id));

		// If the user is found
		if(user != mUsers.end() && stream >> line)
		{
			// Separatting tracks IDs
			vector< string > tracks_ids = sepComma(line);

			// Creating vector of tracks IDs which indicates the tracks the user already likes
			vector< int > likes; 
			likes.resize(tracks_ids.size());

			for(unsigned j = 0; j < likes.size(); j++)
				likes[j] = stoi(tracks_ids[j]);

			(*user)->SetLikes(likes);
		}
	}
	// Closing "likes.csv"
	in_file.close();
}

/******************************************************************
*  Create/update "dislikes.csv" file                              *
******************************************************************/
void RadioStation::UpdateDislikesFile()
{
	// Opening "dislikes.csv" to write
	ofstream out_file("dislikes.csv", ios::out);

	for(unsigned i = 0; i < mUsers.size(); i++)
	{
		vector<int> dislikes = mUsers[i]->GetDislikes();

		// Writing user ID
		out_file << mUsers[i]->GetId() << " ";

		// Writing music tracks ID
		for(unsigned j = 0; j < dislikes.size(); j++)
			out_file << dislikes[j] << ',';

		out_file << endl;
	}

	// Closing "dislikes.csv"
	out_file.close();
}

/******************************************************************
*  Read "dislikes.csv" file                                       *
******************************************************************/
void RadioStation::ReadDislikesFile()
{
	string line;

	// Opening "dislikes.csv" to read
	ifstream in_file("dislikes.csv", ios::in);

	// Reading file
	while(getline(in_file, line))
	{
		int user_id;
		stringstream stream(line);

		// Extracting user ID
		stream >> user_id;

		// Searching for the user in the radio station's users vector
		vector< User* >::iterator user = find_if(mUsers.begin(), mUsers.end(), FindUserById(user_id));

		// If the user is found
		if(user != mUsers.end() && stream >> line)
		{
			// Separatting tracks IDs
			vector< string > tracks_ids = sepComma(line);
			vector< int > dislikes;
			dislikes.resize(tracks_ids.size());

			// Creating vector of tracks IDs which indicates the tracks the user already likes
			for(unsigned j = 0; j < dislikes.size(); j++)
				dislikes[j] = stoi(tracks_ids[j]);

			(*user)->SetDislikes(dislikes);
		}
	}
	// Closing "dislikes.csv"
	in_file.close();
}

/******************************************************************
*  Create/update "topTen.csv" file                                *
******************************************************************/
void RadioStation::UpdateTopTenFile()
{
	// Opening "topTen.csv" file to write
	ofstream out_file("topTen.csv", ios::out);

	// Writing a Music track info per line
	for(unsigned i = 0; i < mTopTen.size(); i++)
		mTopTen[i]->WriteToFile(out_file);

	// Closing file
	out_file.close();
}

/******************************************************************
*  Read "topTen.csv" file                                         *
******************************************************************/
void RadioStation::ReadTopTenFile()
{
	vector< Music* > top_ten;
	vector< string > track;
	string line;

	// Opening file to read
	ifstream in_file("topTen.csv", ios::in);

	// Reading each line
	while(getline(in_file,line))
	{
		// Splitting by commas
		track = sepComma(line);

		// Searching 
		vector<Music*>::const_iterator location = find_if(mMusics.begin(), mMusics.end(), FindMusicTrack(track[0], track[1], track[3], stoi(track[5])));

		// Adding each Music track to a vector
		top_ten.push_back(*location);
	}

	// Closing file
	in_file.close();

	// Setting radio stations top ten musics
	SetTopTen(top_ten);
}

/******************************************************************
*  Create/update "radioStationMusics.csv" file                    *
******************************************************************/
void RadioStation::UpdateMusicsFile()
{
	// Opening "radioStationMusics.csv" file to write
	ofstream out_file("radioStationMusics.csv", ios::out);

	// Writing one Music track per line
	for(unsigned i = 0; i < mMusics.size(); i++)
		mMusics[i]->WriteToFile(out_file);

	// Closing file
	out_file.close();
}

/******************************************************************
*  Read "radioStationMusics.csv" file                             *
******************************************************************/
void RadioStation::ReadMusicsFile()
{
	// Getting data
	vector< Music* > track;
	vector< string> new_track;
	string line;

	// Opening file to read
	ifstream in_file("radioStationMusics.csv", ios::in);

	// Reading each line
	while(getline(in_file,line))
	{
		// Splitting by comma
		new_track = sepComma(line);

		// Adding each Music track to a vector
		track.push_back(new Music(new_track[0],new_track[1],new_track[2],new_track[3],new_track[4], stoi(new_track[5]),stoi(new_track[6]),stoi(new_track[7]),intToBool(stoi(new_track[8]))));
	}

	// Closing file
	in_file.close();

	// Setting final vector
	SetMusics(track);
}

/******************************************************************
*  Create/update "users.csv" with the current user's list         *
******************************************************************/
void RadioStation::UpdateUsersFile()
{
	vector<User*> users(mUsers);

	sort(users.begin(), users.end(), CompareUserId());

	// Opening file to write
	ofstream out_file("users.csv", ios::out);

	// Writting to file
	for(unsigned i = 0; i < users.size(); i++)
		users[i]->WriteToFile(out_file);

	// Closing file
	out_file.close();

}

/******************************************************************
*  Read "users.csv" with the current user's list                  *
******************************************************************/
void RadioStation::ReadUsersFile()
{
	// Getting data
	vector< User* > users(mUsers);
	vector< string> new_users;
	string line;

	// Opening file
	ifstream in_file("users.csv", ios::in);

	while(getline(in_file,line))
	{
		new_users = sepComma(line);
		users.push_back(new User(new_users[0],stoi(new_users[1]),new_users[2][0],new_users[3]));
	}
	in_file.close();
	SetUsers(users);
}

/******************************************************************
*  Read "playListUserXXX.csv" file                                *
*                                                                 *
*  @Param: - a reference to the user which will be loaded         *
******************************************************************/
void RadioStation::ReadPlaylistFile(User &rUser)
{
	// Getting data
	int id = rUser.GetId();
	vector< const Music* > playlist;
	vector< string > track;

	// Building filename
	stringstream  filename;
	filename << "playListUser" << setfill('0') << setw(3) << id << ".csv";

	// Opening file
	ifstream in_file(filename.str(), ios::in);
	string line;

	while(getline(in_file,line))
	{
		// Separating Music track fields
		track = sepComma(line);

		// Searching 
		vector<Music*>::const_iterator location = find_if(mMusics.begin(), mMusics.end(), FindMusicTrack(track[0], track[1], track[3], stoi(track[5])));

		// Adding Music track pointer to the playlist
		playlist.push_back(*location);
	}

	// Closing file;
	in_file.close();

	// Setting User playlist
	rUser.SetPlaylist(playlist);
}

/******************************************************************
*  Print a header: ID, Title, Artist, Author, Album, Music Genre  *
* and year                                                        *
******************************************************************/
void RadioStation::PrintHeader() const
{
	// Printing header
	cout << setw(4) << left << "ID";
	cout << setw(20) << left << "Title";
	cout << setw(10) << left << "Artist";
	cout << setw(10) << left << "Author";
	cout << setw(8) << left << "Album";
	cout << setw(12) << left << "Music-Genre";
	cout << setw(5) << left << "Year";

	// If the user is Admin, show available field
	if(mpCurrentUser->IsAdmin())
		cout << setw(10) << left << "Available";

	cout << endl;
}

/*=========================   ACCESS   ==========================*/
/******************************************************************
*  Set Methods                                                    *
******************************************************************/

void RadioStation::SetMusics(vector< Music* > musics)
{
	mMusics = musics;
}

void RadioStation::SetTopTen(vector< Music* > topTen)
{
	mTopTen = topTen;
}

void RadioStation::SetUsers(vector< User* > users)
{
	mUsers = users;
}

void RadioStation::SetCurrentUser(User &rUser)
{
	mpCurrentUser = &rUser;
}

/******************************************************************
*  Get Methods                                                    *
******************************************************************/

string RadioStation::GetName() const
{
	return mName;
}

vector< Music* > RadioStation::GetMusics() const
{
	return mMusics;
}

vector< Music* > RadioStation::GetTopTen() const
{
	return mTopTen;
}

vector< User* > RadioStation::GetUsers() const
{
	return mUsers;
}

User& RadioStation::rGetCurrentUser() const
{
	return *mpCurrentUser;
}

/*=========================   INQUIRY  ==========================*/

/******************************************************************
*  Check if user exists                                           *
*                                                                 *
*  @Param: - name is the name of the user                         *
*  @Return: true if the user is in the list of radio station's    *
*          users                                                  *
******************************************************************/
bool RadioStation::IsUser(string name) const
{
	for(unsigned i = 0; i < mUsers.size(); i++)
		if(mUsers[i]->GetName() == name)		// If the user is found
			return true;

	// If the user is not found
	return false;
}
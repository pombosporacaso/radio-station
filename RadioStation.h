/******************************************************************
*  Class RadioStation                                             *
*                                                                 *
*  Manage all the information of a radio station                  *
*                                                                 *
*  The radio station has a name, a list of music tracks to        *
* broadcast and a top ten list of the music tracks preferred by   *
* the listeners, a list of users and a pointer to the current     *
* user                                                            *
******************************************************************/

#ifndef RADIO_STATION_h
#define RADIO_STATION_h

#ifndef string
#include <string>
#endif

#ifndef vector
#include <vector>
#endif

#ifndef algorithms
#include <algorithm>
#endif

#include "User.h"
#include "Music.h"
#include "Auxiliary.h"
#include "Exception.h"

//developed by Joao Maia and Jose Oliveira, T6G03

class RadioStation
{
public:
	/*========================= LIFECYCLE ===========================*/

	/******************************************************************
	*  Default constructor                                            *
	******************************************************************/
	RadioStation();

	/******************************************************************
	*  Simple constructor                                             *
	******************************************************************/
	RadioStation(string name);

	/******************************************************************
	*  Destructor                                                     *
	******************************************************************/
	~RadioStation();

	/*========================= OPERATIONS ==========================*/

	/******************************************************************
	*  Main                                                           *
	******************************************************************/
	void Run();

	/******************************************************************
	*  Print to the console a welcoming screen                        *
	*  ID: 0x00                                                       *
	******************************************************************/
	void Preview();

	/******************************************************************
	*  Login menu                                                     *
	*  In this menu, the user can decide whether to login, register   *
	* or quit                                                         *
	*  ID: 0x01                                                       *
	******************************************************************/
	void LoginMenu();

	/******************************************************************
	*  Main menu                                                      *
	*  This menu gives access to the User Menu, Radio Menu and Admin  *
	* menu (if the user is admin)                                     *
	*  ID: 0x02                                                       *
	******************************************************************/
	void MainMenu();


	/******************************************************************
	*  Admin menu                                                     *
	*  In this menu, the admin can manage the list of music tracks of *
	* the radio station                                               *
	*  ID: 0x23                                                       *
	******************************************************************/
	void AdminMenu();

	/******************************************************************
	*  Login                                                          *
	*  Ask for the account name (and password?)                       *
	*  Check if user already exists                                   *
	*  Set the current user                                           *
	*  ID: 0x11                                                       *
	******************************************************************/
	void LoginUser();

	/******************************************************************
	*  Register user                                                  *
	*  To have access to the radio station facilities a user must     *
	* register himself at the application                             *
	*  ID: 0x12                                                       *
	******************************************************************/
	void RegisterUser();

	/******************************************************************
	*  Ask for the parameter to use in the search                     *
	******************************************************************/
	void SearchMenu();

	/******************************************************************
	*  Show the Music tracks of a given Artist / Author / Year        *
	*                                                                 *
	*  @Param: - param: 1 to select by artist                         *
	*                   2 to select by author                         *
	*                   3 to select by year                           *
	******************************************************************/
	void Search(int param);

	/******************************************************************
	*  Order musics by any key: title, artist, year or genre          *
	******************************************************************/
	void OrderMusics();

	/******************************************************************
	*  Search in the radio station's playlist the music with the      *
	* given ID                                                        *
	*                                                                 *
	*  @Return: a reference to the radio station's music with the     *
	*          given ID                                               *
	******************************************************************/
	Music& SelectMusic();

	/******************************************************************
	*  Display all the music track information                        *
	*                                                                 *
	*  @Param: - the track which was selected                         *
	******************************************************************/
	void SelectMusicMenu(Music &track);

	/******************************************************************
	*  Generate, randomly, the set of musics played in a given period *
	*  This set may be reinitialized by the administrator             *
	******************************************************************/
	void GenerateMusics();

	/******************************************************************
	*  Each time that a new set of musics is generated, we have to    *
	* increment the times each music iss played                       *
	*                                                                 *
	*  @Param: - rTrack is a reference to a music from the radio      *
	*          station's playlist                                     *
	******************************************************************/
	void IncTimesPlayed(Music &rTrack);

	/******************************************************************
	*  At the end of the day we need to see which user has the most   *
	* musics with the most hits, in order to give a prize             *
	******************************************************************/
	void GivePrize();

	/******************************************************************
	*  Each time a "like"/"dislike" is changed the top ten list must  *
	* be checked/updated                                              *
	******************************************************************/
	void UpdateTopTen();

	/******************************************************************
	*  Print to the console the Top Ten list                          *
	******************************************************************/
	void ShowTopTen();

	/******************************************************************
	*  ADMIN ONLY                                                     *
	*  Add a music track to the RadioStation's playlist               *
	*  Ask admin for music name, title, artist, author, album, genre  *
	* and year
	******************************************************************/
	void AddMusic();

	/******************************************************************
	*  ADMIN ONLY                                                     *
	*  To edit a radio station's track                                *
	******************************************************************/
	void EditMusic();

	/******************************************************************
	*  ADMIN ONLY                                                     *
	*  To signal as available/unvailable                              *
	******************************************************************/
	void ChangeStatus();

	/******************************************************************
	*  Show all the user's music tracks                               *
	*                                                                 *
	*  @Param: - rUser is a reference to a radio station's user       *
	******************************************************************/
	void ShowUserPlaylist(const User &rUser);

	/******************************************************************
	*  Increment Like counter                                         *
	*                                                                 *
	*  @Param: - rTrack is a reference to a music from the radio      *
	*          station's playlist                                     * 
	******************************************************************/
	void Like(Music &rTrack);

	/******************************************************************
	*  Increment Dislike counter                                      *
	*                                                                 *
	*  @Param: - rTrack is a reference to a music from the radio      *
	*          station's playlist                                     *
	******************************************************************/
	void Dislike(Music &rTrack);

	/******************************************************************
	*  Create/update "likes.csv" file                                 *
	******************************************************************/
	void UpdateLikesFile();

	/******************************************************************
	*  Read "likes.csv" file                                          *
	******************************************************************/
	void ReadLikesFile();

	/******************************************************************
	*  Create/update "dislikes.csv" file                              *
	******************************************************************/
	void UpdateDislikesFile();

	/******************************************************************
	*  Read "dislikes.csv" file                                       *
	******************************************************************/
	void ReadDislikesFile();

	/******************************************************************
	*  Create/update "topTen.csv" file                                *
	******************************************************************/
	void UpdateTopTenFile();

	/******************************************************************
	*  Read "topTen.csv" file                                         *
	******************************************************************/
	void ReadTopTenFile();

	/******************************************************************
	*  Create/update "radioStationMusics.csv" file                    *
	******************************************************************/
	void UpdateMusicsFile();

	/******************************************************************
	*  Read "radioStationMusics.csv" file                             *
	******************************************************************/
	void ReadMusicsFile();

	/******************************************************************
	*  Create/update "users.csv" with the current user's list         *
	******************************************************************/
	void UpdateUsersFile();

	/******************************************************************
	* Read "users.csv" with the current user's list                   *
	******************************************************************/
	void ReadUsersFile();

	/******************************************************************
	*  Read "playListUserXXX.csv" file                                *
	*                                                                 *
	*  @Param: - a reference to the user which will be loaded         *
	******************************************************************/
	void ReadPlaylistFile(User &rUser);

	/******************************************************************
	*  Print a header: ID, Title, Artist, Author, Album, Music Genre  *
	* and year                                                        *
	******************************************************************/
	void PrintHeader() const;

	/*=========================   ACCESS   ==========================*/
	/******************************************************************
	*  Set Methods                                                    *
	******************************************************************/

	void SetMusics(vector< Music* > musics);
	void SetTopTen(vector< Music* > topTen);
	void SetUsers(vector< User* > users);
	void SetCurrentUser(User &rUser); 

	/******************************************************************
	*  Get Methods                                                    *
	******************************************************************/

	string GetName() const;
	vector< Music* > GetMusics() const;
	vector< Music* > GetTopTen() const;
	vector< User* > GetUsers() const;
	User& rGetCurrentUser() const;

	/*=========================   INQUIRY  ==========================*/

	/******************************************************************
	*  Check if user exists                                           *
	*                                                                 *
	*  @Param: - name is the name of the user                         *
	*  @Return: true if the user is in the list of radio station's    *
	*          users                                                  *
	******************************************************************/
	bool IsUser(string name) const;

private:
	// Radio Station's name
	string mName;

	// List of music tracks to broadcast (vector of pointers to Music objects)
	vector< Music* > mMusics;

	// Top ten list of the music tracks preferred by the listeners (vector of pointers to Music objects)
	vector< Music* > mTopTen;

	// List of the radion listeners (vector of pointers to User objects)
	vector< User* > mUsers;

	// Pointer to the current user (user that logged in)
	User* mpCurrentUser;

	// Indicate the current menu
	int mCurrentMenu;
};

#endif // !MUSIC_TRACK_h
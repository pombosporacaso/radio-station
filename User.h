/******************************************************************
*  Class User                                                     *
*                                                                 *
*  Radio Listener                                                 *
*                                                                 *
*  An user is someone that has a unique ID, a name, age, gender   *
* and a play list (list of music track ids)                       *
*  Add and remove musics from the user's playlist                 *
*  Show playlist                                                  *
*  Create/update "playListUserXXX.csv"                            *
******************************************************************/

#ifndef USER_h
#define USER_h

#ifndef string
#include <string>
#endif

#ifndef vector
#include <vector>
#endif

#ifndef fstream
#include <fstream>
#endif

#ifndef sstream
#include <sstream>
#endif

#ifndef iomanip
#include <iomanip>
#endif

#ifndef iostream
#include <iostream>
#endif

#include "Music.h"
#include "Auxiliary.h"

using namespace std;

//developed by Joao Maia and Jose Oliveira, T6G03

class User
{
public: 
	/*========================= LIFECYCLE ===========================*/

	/******************************************************************
	*  Default constructor                                            *
	******************************************************************/
	User();

	/******************************************************************
	*  Simple constructor                                             *
	******************************************************************/
	User(string name, int age, char gender, string password);

	/******************************************************************
	*  Destructor                                                     *
	******************************************************************/
	~User();

	/*========================= OPERATIONS ==========================*/
	/******************************************************************
	*  Add a music to the user's playlist                             *
	******************************************************************/
	void AddMusic(const Music &rTrack);

	/******************************************************************
	*  Remove a music from the user's playlist                        *
	******************************************************************/
	void RemoveMusic(const Music &rTrack);

	/******************************************************************
	*  Show all the user's music tracks                               *
	******************************************************************/
	void ShowPlaylist() const;

	/******************************************************************
	*  Write to a file the user info                                  *
	*                                                                 *
	*  @Param: - rOutFile is the reference to an ofstream object.     *
	*          It will specify the targeted file and how it writes    *
	*          (out or append)                                        *
	******************************************************************/
	void WriteToFile(ofstream &rOutFile) const;

	/******************************************************************
	*  Write the current user's playlist on "playListUserXXX.csv"     *
	******************************************************************/
	void UpdatePlaylistFile();

	/*=========================   ACCESS   ==========================*/
	/******************************************************************
	*  Set Methods                                                    *
	******************************************************************/
	
	void SetPassword(string password);
	void SetPlaylist(vector< const Music* > playlist);
	void SetAdmin(bool isAdmin);
	void SetLikes(vector< int > likes);
	void SetDislikes(vector< int > dislikes);

	/******************************************************************
	*  Get Methods                                                   *
	******************************************************************/

	int GetId() const;
	string GetName() const;
	string GetPassword() const;
	int GetAge() const;
	char GetGender() const;
	vector< const Music* > GetPlaylist() const;
	vector< int > GetLikes() const;
	vector< int > GetDislikes() const; 

	/*=========================   INQUIRY  ==========================*/

	/******************************************************************
	*  @Return: true if user is admin                                 *
	******************************************************************/
	bool IsAdmin() const;

private:
	// User ID
	int mId;

	// User name
	string mName;

	// User password
	string mPassword;

	// User age
	int mAge;

	// User gender (M/F)
	char mGender;

	//Validates if user is admin or not
	bool mIsAdmin;

	vector<int> mLikes;
	vector<int> mDislikes;

	// User's playlist (vector of pointers to Music objects)
	vector< const Music* > mPlaylist;

	// ID Counter (the ID of the next User to be created: last user ID + 1)
	static int msIdCounter;
};

struct CompareUserId
{
	bool operator() (User* user1, User* user2)
	{
		return user1->GetId() < user2->GetId();
	}
};

struct CompareUserScore
{
	bool operator() (User* user1, User* user2)
	{
		int score1 = 0;
		vector< const Music* > playlist1(user1->GetPlaylist());

		for(unsigned i = 0; i < playlist1.size(); i++)
			score1 += playlist1[i]->GetTimesPlayed();

		int score2 = 0;
		vector< const Music* > playlist2(user2->GetPlaylist());

		for(unsigned i = 0; i < playlist2.size(); i++)
			score2 += playlist2[i]->GetTimesPlayed();

		return score1 > score2;
	}
};

struct FindUserById
{
	FindUserById(int userId) : userId(userId) {}

	bool operator() (User* user)
	{
		return userId == user->GetId();
	}

	int userId;
};

#endif // !USER_h
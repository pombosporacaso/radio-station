#include "User.h"

using namespace std;

int User::msIdCounter = 0;

//developed by Joao Maia and Jose Oliveira, T6G03

/*========================= LIFECYCLE ===========================*/

/******************************************************************
*  Default constructor                                            *
******************************************************************/
User::User()
{
	// Default name, age and gender
	mName = "Unknown";
	mAge = 0;
	mGender = 'N';
	mPassword= "123";

	// Empty Playlist
	vector< const Music* > playlist(0);
	mPlaylist = playlist;

	// Giving and incrementing ID
	msIdCounter++;
	mId = msIdCounter;

	// Previleges
	mIsAdmin = false;
}

/******************************************************************
*  Simple constructor                                             *
******************************************************************/
User::User(string name, int age, char gender, string password)
{
	// Setting name, age and gender
	mName=name;
	mAge=age;
	mGender=gender;
	mPassword=password;

	// Empty Playlist
	vector< const Music* > playlist(0);
	mPlaylist = playlist;

	// Giving ID and incrementing ID counter
	msIdCounter++;
	mId = msIdCounter;

	// Previleges
	mIsAdmin = false;
}

/******************************************************************
*  Destructor                                                     *
******************************************************************/
User::~User()
{
	UpdatePlaylistFile();
}

/*========================= OPERATIONS ==========================*/

/******************************************************************
*  Add a music to the user's playlist                             *
******************************************************************/
void User::AddMusic(const Music &rTrack)
{
	// Searching for the pointer to the Music track
	vector< const Music* >::iterator location = find(mPlaylist.begin(), mPlaylist.end(), &rTrack); 

	// If the Music track isn't in the playlist, then adding to the back
	if(location == mPlaylist.end())
	{
		mPlaylist.push_back(&rTrack);
		cout<<"Music track successfully added to the playlist";
	}

	else
		cout<<"Music track is already in the playlist";
	_getch();
}

/******************************************************************
*  Remove a music from the user's playlist                        *
******************************************************************/
void User::RemoveMusic(const Music &rTrack)
{
	// Searching for the pointer to the Music track
	vector< const Music* >::iterator location = find(mPlaylist.begin(), mPlaylist.end(), &rTrack);

	// If the Music reference is found, erasing it
	if(location != mPlaylist.end())
	{
		mPlaylist.erase(location);
		cout<<"Music track successfully removed from the playlist";
	}

	else
		cout<<"Music track wasn't found";

	_getch();
}

/******************************************************************
*  Show all the user's music tracks                               *
******************************************************************/
void User::ShowPlaylist() const
{
	// Going through all the Music track objects in the user playlist
	for(unsigned i = 0; i < mPlaylist.size(); i++)
	{
		// Showing music track
		mPlaylist[i]->ShowMusic();

		// Showing availability info if the user is admin
		if(IsAdmin())
		{
			// Yes
			if(mPlaylist[i]->IsAvailable())
			{
				cout.width(10); 
				cout << left << "Yes";
			}
			// No
			else
			{
				cout.width(10); 
				cout << left << "No";
			}
		}
		cout << endl;
	}
}

/******************************************************************
*  Write to a file the user info                                  *
*                                                                 *
*  @Param: - rOutFile is the reference to an ofstream object.     *
*          It will specify the targeted file and how it writes    *
*          (out or append)                                        *
******************************************************************/
void User::WriteToFile(ofstream &rOutFile) const
{
	if(!IsAdmin())
	{
		rOutFile << GetName() << ",";		// Name
		rOutFile << GetAge() << ",";		// Age
		rOutFile << GetGender() << ",";		// Gender
		rOutFile << GetPassword() << endl;	// Password
	}
}

/******************************************************************
*  Write the current user's playlist on "playListUserXXX.csv"     *
******************************************************************/
void User::UpdatePlaylistFile()
{
	// Getting data
	int id = GetId();
	vector< const Music* > playlist = GetPlaylist();

	// Building filename
	stringstream  filename;
	filename << "playListUser" << setfill('0') << setw(3) << id << ".csv";

	// Opening file
	ofstream out_file(filename.str(), ios::out);

	// Writing to the file
	for(unsigned i = 0; i < playlist.size(); i++)
		mPlaylist[i]->WriteToFile(out_file);

	// Closing the file
	out_file.close();
}

/*=========================   ACCESS   ==========================*/

/******************************************************************
*  Set Methods                                                    *
******************************************************************/

void User::SetPassword(string password)
{
	mPassword= password;
}

void User::SetPlaylist(vector< const Music* > playlist)
{
	mPlaylist = playlist;
}

void User::SetAdmin(bool isAdmin)
{
	mIsAdmin = isAdmin;
}

void User::SetLikes(vector< int > likes)
{
	mLikes = likes;
}

void User::SetDislikes(vector< int > dislikes)
{
	mDislikes = dislikes;
}

/******************************************************************
*  Get Methods                                                   *
******************************************************************/

int User::GetId() const
{
	return mId;
}

string User::GetPassword() const
{
	return mPassword;
}

string User::GetName() const
{
	return mName;
}

int User::GetAge() const
{
	return mAge;
}

char User::GetGender() const
{
	return mGender;
}

vector< const Music* > User::GetPlaylist() const
{
	return mPlaylist;
}

vector< int > User::GetLikes() const
{
	return mLikes;
}

vector< int > User::GetDislikes() const
{
	return mDislikes;
}

/*=========================   INQUIRY  ==========================*/

/******************************************************************
*  @Return: true if user is admin                                 *
******************************************************************/
bool User::IsAdmin() const
{
	return mIsAdmin;
}

#ifndef RADIO_EXCEPTION_h
#define RADIO_EXCEPTION_h

#include <stdexcept>

using namespace std;

//developed by Joao Maia and Jose Oliveira, T6G03

class RadioException : public exception
{
public:
	/******************************************************************
	*  Simple constructor                                             *
	*                                                                 *
	*  @Param: - error message to display                             *
	*          - menu to go after the exception is thrown             *
	*          - if quit = true, the error message will not be        *
	*          displayed                                              *
	******************************************************************/
	RadioException(const char* msg, int redirectToMenu = -1, bool quit = false);

	/******************************************************************
	*  Get methods                                                    *
	******************************************************************/

	const char* GetMsg() const;

	int Menu() const;

	bool Quit() const;

private:
	const char* mMsg;
	int mMenu;
	bool mQuit;
};

#endif
#ifndef AUXILIARY_h
#define AUXILIARY_h

#ifndef iostream
#include <iostream>
#endif

#ifndef string
#include <string>
#endif

#ifndef sstream
#include <sstream>
#endif

#ifndef fstream
#include <fstream>
#endif

#ifndef vector
#include <vector>
#endif

#ifndef iomanip
#include <iomanip>
#endif

#ifndef conio
#include <conio.h>
#endif

#ifndef Windows
#include <Windows.h>
#endif

#include "Exception.h"

using namespace std;

//developed by Joao Maia and Jose Oliveira, T6G03

/******************************************************************
*  Main function for validation of user input                     *
*                                                                 *
*  @Param: - type can be 's' for string, 'd' for decimal or 'c'   *
*          for a single char                                      *
*          - optinal except = true (will throw exception if       *
*          ctrl+z is pressed                                      *
*          - the id of the menu to return if ctrl+z is pressed    *
*  @Return: user input or "^Z" if ctrl+z is pressed and except =  *
*          false                                                  *
******************************************************************/
string input(char type, bool exception = true, int menu = -1);

/******************************************************************
*  Used for input and validation of the gender                    *
*                                                                 *
*  @Param: - optional parameter except = true, throws exception   *
*          if ctrl+z is pressed. Else returns a '^Z' char         *
*          - menu to return of ctrl+z is pressed                  *
*  @Return: 'M', 'F' or '^Z'                                      *
******************************************************************/
char genderInput(bool except = true, int menu = -1);

/******************************************************************
*  Check if the key is valid                                      *
*                                                                 *
*  @Param: - key pressed by the user                              *
*          - type of input ('s' for string, 'd' for decimal, 'c'  *
*          for a single char                                      *
*  @Return: true if the key is valid                              *
******************************************************************/
bool isValidKey(int key, char type);

/******************************************************************
*  Used to quit from the user input                               *
*                                                                 *
*  @Param: - if exception == true, throws and exception           *
*  @Exception: RadioException("Quit", -1, true)                   *
*  @Return: string "^Z" if exception == false                     *
******************************************************************/
string quit(bool except, int menu, char type = 's');

/******************************************************************
*  Split the string by comma (',') and returns a vector of        *
* strings                                                         *
*                                                                 *
*  @Param: - line: string to split                                *
*  @Return: vector of splitted strings                            *
******************************************************************/
vector<string> sepComma(string line);

/******************************************************************
*  Convert an int to a bool                                       *
*                                                                 *
*  @Param: - integer (0 or 1)                                     *
*  @Return: true if num = 1                                       *
******************************************************************/
bool intToBool(int num);

/******************************************************************
*  Get the current cursor position                                *
*                                                                 *
*  @Return: COORD object                                          *
******************************************************************/
COORD getCursorPosition();

/******************************************************************
*  Get the current cursor position and clear the screen line      *
*                                                                 *
*  @Param: - position: cursor position destination                *
*          - size: number of characters to erase                  *
******************************************************************/
void goToCoord(COORD position, int size = -1);

/******************************************************************
*  Clear the console screen                                       *
*  By: JAS - Abr/2011                                             *
******************************************************************/
void clrscr(void);

#endif
#include "Music.h"

int Music::msIdCounter = 0;

//developed by Joao Maia and Jose Oliveira, T6G03

/*========================= LIFECYCLE ===========================*/

/******************************************************************
*  Default constructor                                            *
******************************************************************/
Music::Music()
{
	// Giving and incrementing ID
	msIdCounter++;
	mId = msIdCounter;

	mAvailable = true;

	// Setting default members
	mTitle = "Unknown";
	mAuthor = "Unknown";
	mArtist = "Unknown";
	mAlbum = "Unknown";
	mMusicGenre = "Unknown";
	mYear = 0;

	// Setting Likes, Dislikes and Times Played to 0
	mLikes = 0;
	mDislikes = 0;
	mTimesPlayed = 0;
}

/******************************************************************
*  Simple constructor                                             *
******************************************************************/
Music::Music(string title, string artist, string author, string album, string musicGenre, int year, int likes, int dislikes, bool isAvailable)
{
	// Giving and incrementing ID
	msIdCounter++;
	mId = msIdCounter;

	mAvailable = true;

	// Setting members
	SetTitle(title);
	SetAuthor(author);
	SetArtist(artist);
	SetAlbum(album);
	SetMusicGenre(musicGenre);
	SetYear(year);
	mLikes = likes;
	mDislikes = dislikes;
	mAvailable = isAvailable;

	// Setting Times Played to 0
	mTimesPlayed = 0;
}

/******************************************************************
*  Destructor                                                     *
******************************************************************/
Music::~Music()
{
	ofstream out_file("radioStationMusics.csv", ios::app);
	WriteToFile(out_file);
	out_file.close();
}


/*========================= OPERATIONS ==========================*/

/******************************************************************
*  Show Music: Title, Artist, Author, Albun, Genre, Year, Likes,  *
* Dislikes                                                        *
******************************************************************/
void Music::ShowMusic() const
{
	printf("%-4.3d", GetId());
	printf("%-20.19s", GetTitle().c_str());
	printf("%-10.9s", GetArtist().c_str());
	printf("%-10.9s", GetAuthor().c_str());
	printf("%-8.7s", GetAlbum().c_str());
	printf("%-12.11s", GetMusicGenre().c_str());
	printf("%-4.3d ", GetYear());
}

/******************************************************************
*  Write to a file the music info                                 *
*                                                                 *
*  @Param: - rOutFile is the reference to an ofstream object.     *
*          It will specify the targeted file and how it writes    *
*          (out or append)                                        *
******************************************************************/
void Music::WriteToFile(ofstream &rOutFile) const
{
	rOutFile<< GetTitle() << ",";			// Title
	rOutFile<< GetArtist() << ",";			// Artist
	rOutFile<< GetAuthor() << ",";			// Author
	rOutFile<< GetAlbum() << ",";			// Album
	rOutFile<< GetMusicGenre() << ",";		// Music Genre
	rOutFile<< GetYear() << ",";			// Year
	rOutFile<< GetLikes() << ",";			// Likes
	rOutFile<< GetDislikes() << ",";		// Dislikes
	rOutFile<< IsAvailable() << endl;		// Availability
}

/*=========================   ACCESS   ==========================*/
/******************************************************************
*  Set Methods                                                    *
******************************************************************/

void Music::SetId(int id)
{
	mId = id;
}

void Music::SetTitle(string title)
{
	if(title != "^Z")
	{
		// Checking if first letter is upper case
		if(title[0] > 0x60 && title[0] < 0x7B)
			title[0] -= 0x20;	// Converting first letter to upper case

		mTitle = title;
	}
}

void Music::SetArtist(string artist)
{
	if(artist != "^Z")
	{
		// Checking if first letter is upper case
		if(artist[0] > 0x60 && artist[0] < 0x7B)
			artist[0] -= 0x20;	// Converting first letter to upper case

		mArtist = artist;
	}
}

void Music::SetAuthor(string author)
{
	if(author != "^Z")
	{
		// Checking if first letter is upper case
		if(author[0] > 0x60 && author[0] < 0x7B)
			author[0] -= 0x20;	// Converting first letter to upper case

		mAuthor = author;
	}
}

void Music::SetAlbum(string album)
{
	if(album != "^Z")
	{
		// Checking if first letter is upper case
		if(album[0] > 0x60 && album[0] < 0x7B)
			album[0] -= 0x20;	// Converting first letter to upper case

		mAlbum = album;
	}
}

void Music::SetMusicGenre(string musicGenre)
{
	if(musicGenre != "^Z")
	{
		// Checking if first letter is upper case
		if(musicGenre[0] > 0x60 && musicGenre[0] < 0x7B)
			musicGenre[0] -= 0x20;	// Converting first letter to upper case

		mMusicGenre = musicGenre;
	}
}

void Music::SetYear(int year)
{
	if(year != -1)
		mYear = year;
}

void Music::SetLikes(int likes)
{
	mLikes = likes;
}

void Music::SetDislikes(int dislikes)
{
	mDislikes = dislikes;
}

void Music::SetTimesPlayed(int timesPlayed)
{
	mTimesPlayed = timesPlayed;
}

void Music::SetAvailable(bool available)
{
	mAvailable = available;
}


/******************************************************************
*  Get Methods                                                    *
******************************************************************/

int Music::GetId() const
{
	return mId;
}

string Music::GetTitle() const
{
	return mTitle;
}

string Music::GetArtist() const
{
	return mArtist;
}

string Music::GetAuthor() const
{
	return mAuthor;
}

string Music::GetAlbum() const
{
	return mAlbum;
}

string Music::GetMusicGenre() const
{
	return mMusicGenre;
}

int Music::GetYear() const
{
	return mYear;
}

int Music::GetLikes() const
{
	return mLikes;
}

int Music::GetDislikes() const
{
	return mDislikes;
}

int Music::GetTimesPlayed() const
{
	return mTimesPlayed;
}

/*=========================   INQUIRY  ==========================*/

/******************************************************************
*  Check if music is available                                    *
*                                                                 *
*  @Return: true if the music is available in the radio station   *
******************************************************************/
bool Music::IsAvailable() const
{
	return mAvailable;
}

/******************************************************************
*  Class Music                                                    *
*                                                                 *
*  A music track has an ID, a title, artist, author, album, music *
* genre, year, number of "likes", number of "dislikes" and the    *
* number of times it was played in the radio station              *
******************************************************************/

#ifndef MUSIC_TRACK_h
#define MUSIC_TRACK_h

#ifndef string
#include <string>
#endif

#ifndef vector
#include <vector>
#endif

#ifndef fstream
#include <fstream>
#endif

#ifndef iostream
#include <iostream>
#endif

#include "Exception.h"


using namespace std;

//developed by Joao Maia and Jose Oliveira, T6G03

class Music
{
public:
	/*========================= LIFECYCLE ===========================*/

	/******************************************************************
	*  Default constructor                                            *
	******************************************************************/
	Music();

	/******************************************************************
	*  Simple constructor                                             *
	******************************************************************/
	Music(string title, string artist, string author, string album, string musicGenre, int year, int likes, int dislikes, bool IsAvailable);

	/******************************************************************
	*  Destructor                                                     *
	******************************************************************/
	~Music();

	/*========================= OPERATIONS ==========================*/

	/******************************************************************
	*  Show Music: Title, Artist, Author, Albun, Genre, Year, Likes,  *
	* Dislikes                                                        *
	******************************************************************/
	void ShowMusic() const;

	/******************************************************************
	*  Write to a file the music info                                 *
	*                                                                 *
	*  @Param: - rOutFile is the reference to an ofstream object.     *
	*          It will specify the targeted file and how it writes    *
	*          (out or append)                                        *
	******************************************************************/
	void WriteToFile(ofstream &rOutFile) const;

	/*=========================   ACCESS   ==========================*/
	/******************************************************************
	*  Set Methods                                                    *
	******************************************************************/

	void SetId(int id);
	void SetTitle(string title);
	void SetArtist(string artist);
	void SetAuthor(string author);
	void SetAlbum(string album);
	void SetMusicGenre(string musicGenre);
	void SetYear(int year);
	void SetLikes(int likes);
	void SetDislikes(int dislikes);
	void SetTimesPlayed(int timesPlayed);
	void SetAvailable(bool available);

	/******************************************************************
	*  Get Methods                                                    *
	******************************************************************/

	int GetId() const;
	string GetTitle() const;
	string GetArtist() const;
	string GetAuthor() const;
	string GetAlbum() const;
	string GetMusicGenre() const;
	int GetYear() const;
	int GetLikes() const;
	int GetDislikes() const;
	int GetTimesPlayed() const;

	/*=========================   INQUIRY  ==========================*/

	/******************************************************************
	*  Check if music is available                                    *
	*                                                                 *
	*  @Return: true if the music is available in the radio station   *
	******************************************************************/
	bool IsAvailable() const;

private:
	// Music ID
	int mId;

	// Music title
	string mTitle;

	// Music artist
	string mArtist;

	// Music author
	string mAuthor;

	// Music album
	string mAlbum;

	// Music genre
	string mMusicGenre;

	// Music year
	int mYear;

	// Number of likes
	int mLikes;

	// Number of dislikes
	int mDislikes;

	// Number of times the music track was played in the radio station
	int mTimesPlayed;

	// ID Counter (the ID of the next Track to be created: last user ID + 1) 
	static int msIdCounter;

	// Status of the music
	bool mAvailable;
};

/*=========================   COMPARE  ==========================*/

/******************************************************************
*  Compare Music Score (difference between likes and dislikes)    *
******************************************************************/
struct CompareScore
{
	bool operator() (Music* music1, Music* music2)
	{
		// Calculating difference between likes and dislikes for each music
		int delta1 = music1->GetLikes() - music1->GetDislikes();
		int delta2 = music2->GetLikes() - music2->GetDislikes();

		// Returning true if music1 has less score
		return delta1 > delta2;
	}
};

/******************************************************************
*  Compare Music Titles                                           *
******************************************************************/
struct CompareTitle
{
	bool operator() (Music* track1, Music* track2) 
	{
		return track1->GetTitle() < track2->GetTitle();
	}
};

/******************************************************************
*  Compare Music Artists                                          *
******************************************************************/
struct CompareArtist
{
	bool operator() (Music* track1, Music* track2)
	{
		return track1->GetArtist() < track2->GetArtist();
	}
};

/******************************************************************
*  Compare Music Years                                            *
******************************************************************/
struct CompareYear
{
	bool operator() (Music* track1, Music* track2)
	{
		return track1->GetYear() < track2->GetYear();
	}
};

/******************************************************************
*  Compare Music Genre                                            *
******************************************************************/
struct CompareMusicGenre
{
	bool operator() (Music* track1, Music* track2)
	{
		return track1->GetMusicGenre() < track2->GetMusicGenre();
	}
};

/******************************************************************
*  Find a Music track given a title, artist, album and year       *
******************************************************************/
struct FindMusicTrack
{
	FindMusicTrack(string title, string artist, string album, int year) : title(title), artist(artist), album(album), year(year) {}

	bool operator() (Music* track)
	{
		return track->GetTitle() == title
			&& track->GetArtist() == artist
			&& track->GetAlbum() == album
			&& track->GetYear() == year;
	}

	string title, artist, album;
	int year;
};

/******************************************************************
*  Find a Music track given the ID                                *
******************************************************************/
struct FindId
{
	FindId(int id) : id(id) {}

	bool operator() (const Music* track)
	{
		return id == track->GetId();
	}

	int id;

};

#endif // !MUSIC_TRACK_h
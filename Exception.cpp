#include "Exception.h"

//developed by Joao Maia and Jose Oliveira, T6G03

/******************************************************************
*  Simple constructor                                             *
*                                                                 *
*  @Param: - error message to display                             *
*          - menu to go after the exception is thrown             *
*          - if quit = true, the error message will not be        *
*          displayed                                              *
******************************************************************/
RadioException::RadioException(const char* msg, int redirectToMenu, bool quit)
{
	mMsg = msg;
	mMenu = redirectToMenu;
	mQuit = quit;
}


/******************************************************************
*  Get methods                                                    *
******************************************************************/

int RadioException::Menu() const
{
	return mMenu;
}

bool RadioException::Quit() const
{
	return mQuit;
}

const char* RadioException::GetMsg() const
{
	return mMsg;
}
